// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car.
// Then log the car's year, make, and model in the console log in the format of: 
function findCar(inventory,id)
{

    if(inventory===null||inventory===undefined||id===null||id===undefined||!Array.isArray(inventory)||typeof id!=='number')
    {
        return [];
    }  
    let carDetail;

    for(let index=0;index<inventory.length;index++)
    {
        if(inventory[index].id===id)
        {
            carDetail=inventory[index];
        }
    }
    return carDetail;
}
module.exports=findCar;