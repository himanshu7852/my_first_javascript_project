//problem3

function sortCarModel(inventory)
{
    let listOfCarModels=[];
    for(let index=0;index<inventory.length;index++)
    {
        let car_Mod=inventory[index].car_model;
        car_Mod=car_Mod.toLowerCase();
        listOfCarModels.push(car_Mod);
    }
    return listOfCarModels.sort();
    
}


module.exports=sortCarModel;
